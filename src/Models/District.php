<?php

namespace HMT\VNAU\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class District
 *
 * @property  int         $id
 * @property  string      $name
 * @property  Collection  $wards
 * @property  City        $city
 */
class District extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return config('vnau.table_prefix') . 'districts';
    }

    /**
     * Get the wards for the district.
     */
    public function wards(): HasMany
    {
        return $this->hasMany(Ward::class, 'district_id', 'id');
    }

    /**
     * Get the city that owns the district.
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
