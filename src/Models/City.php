<?php

namespace HMT\VNAU\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 *
 * @property  int         $id
 * @property  string      $name
 * @property  Collection  $districts
 */
class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return config('vnau.table_prefix') . 'cities';
    }

    /**
     * Get the districts for the city.
     */
    public function districts()
    {
        return $this->hasMany(District::class, 'city_id', 'id');
    }
}
