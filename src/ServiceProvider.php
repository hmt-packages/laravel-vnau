<?php

namespace HMT\VNAU;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/vnau.php', 'vnau'
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../config/vnau.php' => config_path('vnau.php'),
            __DIR__.'/../data' => resource_path('data'),
        ], 'vnau-public');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
