<?php

namespace HMT\VNAU\Imports;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VNAUImport implements ToArray, WithHeadingRow
{
    private Model $city;
    private Model $district;
    private Model $ward;

    public function __construct()
    {
        $this->city = app(config('vnau.models.city'));
        $this->district = app(config('vnau.models.district'));
        $this->ward = app(config('vnau.models.ward'));
    }

    public function array(array $array)
    {
        $temp = [
            'cities' => [],
            'districts' => [],
            'wards' => [],
        ];

        foreach ($array as $row) {
            $city = $this->city->updateOrCreate(
                ['id' => (int)$row['ma_tp']],
                ['name' => $row['tinh_thanh_pho']]
            );

            if (! in_array($city->id, $temp['cities'])) {
                array_push($temp['cities'], $city->id);
            }

            $district = $city->districts()->updateOrCreate(
                ['id' => (int)$row['ma_qh']],
                ['name' => $row['quan_huyen']]
            );

            if (! in_array($district->id, $temp['districts'])) {
                array_push($temp['districts'], $district->id);
            }

            if ($row['phuong_xa']) {
                $ward = $district->wards()->updateOrCreate(
                    ['id' => (int)$row['ma_px']],
                    ['name' => $row['phuong_xa']]
                );

                if (! in_array($ward->id, $temp['wards'])) {
                    array_push($temp['wards'], $ward->id);
                }
            }
        }

        $this->city->whereNotIn('id', $temp['wards'])->delete();
        $this->district->whereNotIn('id', $temp['districts'])->delete();
        $this->ward->whereNotIn('id', $temp['cities'])->delete();
    }
}
