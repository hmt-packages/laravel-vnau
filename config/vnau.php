<?php

return [
    'table_prefix' => 'vn_',

    'models' => [
        'city' => \HMT\VNAU\Models\City::class,
        'district' => \HMT\VNAU\Models\District::class,
        'ward' => \HMT\VNAU\Models\Ward::class,
    ],
];
