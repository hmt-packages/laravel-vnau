<?php

namespace HMT\VNAU\Database\Seeders;

use HMT\VNAU\Imports\VNAUImport;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class VNAUSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $filePath = resource_path('data/vnau.xls');

        if (File::exists($filePath)) {
            Excel::import(new VNAUImport(), $filePath);
        }
    }
}
