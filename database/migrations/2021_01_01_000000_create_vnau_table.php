<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private string $cityTable;
    private string $districtTable;
    private string $wardTable;

    public function __construct()
    {
        $this->cityTable = app(config('vnau.models.city'))->getTable();
        $this->districtTable = app(config('vnau.models.district'))->getTable();
        $this->wardTable = app(config('vnau.models.ward'))->getTable();
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->cityTable, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create($this->districtTable, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('city_id')
                ->constrained($this->cityTable)
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create($this->wardTable, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('district_id')
                ->constrained($this->districtTable)
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->wardTable);
        Schema::dropIfExists($this->districtTable);
        Schema::dropIfExists($this->cityTable);
    }
};
